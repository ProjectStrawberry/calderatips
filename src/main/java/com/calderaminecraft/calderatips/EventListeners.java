package com.calderaminecraft.calderatips;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.Map;

public class EventListeners implements Listener {
    private CalderaTips plugin;
    EventListeners(CalderaTips p) {
        plugin = p;
    }
    @EventHandler
    public void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();
        String command;
        if (message.contains(" ")) {
            command = message.substring(1, message.indexOf(" "));
        } else {
            command = message.substring(1);
        }

        if (plugin.cmdCache.containsKey(command)) {
            player.performCommand("faq question " + plugin.cmdCache.get(command));
        }
    }

    @EventHandler
    public void AsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        String message = event.getMessage();

        if (plugin.disabledUsers.contains(player.getUniqueId())) return;

        for (Map.Entry<String, String> entry : plugin.triggerCache.entrySet()) {
            String regex = entry.getKey();
            final String questionId = entry.getValue();

            if (message.toLowerCase().matches(regex)) {
                BukkitTask task = new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (player == null || !player.isOnline()) return;
                        player.performCommand("faq question " + questionId);
                    }
                }.runTask(plugin);
                return;
            }
        }
    }
}
