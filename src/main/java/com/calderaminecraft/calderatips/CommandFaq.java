package com.calderaminecraft.calderatips;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class CommandFaq implements CommandExecutor {
    private CalderaTips plugin;
    CommandFaq(CalderaTips p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }

        if (!sender.hasPermission("caldera.tips.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        Player player = (Player) sender;

        if (args.length < 1) {
            args = new String[] {"main"};
        }

        if (args[0].equals("main")) {
            int pageNum = 1;

            if (args.length > 1) {
                try {
                    pageNum = Integer.parseInt(args[1]);
                } catch (NumberFormatException ignored) {}
                if (pageNum < 1) pageNum = 1;
            }

            try {
                Connection c = plugin.getConnection();
                Statement stmt = c.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT count(*) FROM tips WHERE parentId=\"root\"");

                if (rs.getInt("count(*)") < 1)  {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no questions added.");
                    stmt.close();
                    return true;
                }

                stmt = c.createStatement();
                rs = stmt.executeQuery("SELECT * FROM tips WHERE parentId=\"root\" ORDER BY priority ASC");

                List<BaseComponent> messages = new ArrayList<>();

                boolean hasPerm = sender.hasPermission("caldera.tips.admin");

                while (rs.next()) {
                    TextComponent message = new TextComponent(rs.getString("categoryName"));
                    message.setColor(ChatColor.GOLD.asBungee());
                    message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/faq category " + rs.getString("categoryId")));
                    if (hasPerm) {
                        TextComponent id = new TextComponent(ChatColor.GRAY + " [" + rs.getString("categoryId") + "]");
                        TextComponent priority = new TextComponent(ChatColor.GRAY + " [" + rs.getInt("priority") + "]");
                        TextComponent removeCategory = new  TextComponent(ChatColor.DARK_RED + " [X]");
                        removeCategory.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removecategory " + rs.getString("categoryId")));
                        message.addExtra(id);
                        message.addExtra(priority);
                        message.addExtra(removeCategory);
                    }
                    messages.add(message);
                }

                List<BaseComponent> page = plugin.pageify(messages, pageNum, "/faq main");
                for (BaseComponent curMessage : page) player.spigot().sendMessage(curMessage);
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/faq main: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (args[0].equals("category")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /faq category <category id>");
                return true;
            }

            String categoryId = args[1];
            int pageNumber = 1;

            if (args.length > 2) {
                try {
                    pageNumber = Integer.parseInt(args[2]);
                    if (pageNumber < 1) pageNumber = 1;
                } catch (NumberFormatException ignored) {}
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE categoryId=?");
                pstmt.setString(1, categoryId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.DARK_GREEN + "This category has no questions added.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM tips WHERE categoryId=? AND questionId NOT NULL OR parentId=? ORDER BY priority ASC");
                pstmt.setString(1, categoryId);
                pstmt.setString(2, categoryId);
                rs = pstmt.executeQuery();

                boolean hasPerm = sender.hasPermission("caldera.tips.admin");
                List<BaseComponent> messages = new ArrayList<>();

                while (rs.next()) {
                    TextComponent message = new TextComponent((rs.getString("question") == null ? rs.getString("categoryName") : rs.getString("question")));
                    message.setColor(ChatColor.GOLD.asBungee());
                    if (rs.getString("questionId") != null) {
                        message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/faq question " + rs.getString("questionId")));
                    } else {
                        message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/faq category " + rs.getString("categoryId")));
                    }
                    if (hasPerm) {
                        if (rs.getString("questionId") != null) {
                            TextComponent id = new TextComponent(ChatColor.GRAY + " [" + rs.getString("questionId") + "]");
                            TextComponent priority = new TextComponent(ChatColor.GRAY + " [" + rs.getInt("priority") + "]");
                            TextComponent remove = new TextComponent(ChatColor.DARK_RED + " [X]");
                            remove.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removequestion " + rs.getString("questionId")));
                            message.addExtra(id);
                            message.addExtra(priority);
                            message.addExtra(remove);
                        } else {
                            TextComponent id = new TextComponent(ChatColor.GRAY + " [" + rs.getString("categoryId") + "]");
                            TextComponent priority = new TextComponent(ChatColor.GRAY + " [" + rs.getInt("priority") + "]");
                            TextComponent remove = new TextComponent(ChatColor.DARK_RED  + " [X]");
                            remove.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removecategory " + rs.getString("categoryId")));
                            message.addExtra(id);
                            message.addExtra(priority);
                            message.addExtra(remove);
                        }
                    }
                    messages.add(message);
                }

                List<BaseComponent> page = plugin.pageify(messages, pageNumber, "/faq category " + categoryId);

                for (BaseComponent message : page) player.spigot().sendMessage(message);

                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/faq category: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }

        if (args[0].equals("survey")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /faq survey <yes|no|bad> <question id>");
                return true;
            }

            if (args[1].equals("yes") || args[1].equals("no") || args[1].equals("bad")) {
                String questionId = args[2];

                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                    pstmt.setString(1, questionId);
                    ResultSet rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no question with this id.");
                        pstmt.close();
                        return true;
                    }

                    pstmt = c.prepareStatement("UPDATE tips SET vote" + args[1] + "= vote" + args[1] + " + 1 WHERE questionId=?");
                    pstmt.setString(1, questionId);
                    pstmt.executeUpdate();
                    pstmt.close();

                    sender.sendMessage(ChatColor.DARK_GREEN + "You've voted for this answer. Thank you for your input.");
                    return true;
                } catch (Exception e) {
                    plugin.getLogger().severe("/faq survey: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                    return true;
                }
            }
        }

        if (args[0].equals("question")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /faq question <question id>");
                return true;
            }

            String questionId = args[1];

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That question does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                rs = pstmt.executeQuery();

                if (rs.getString("answer") == null) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That question does not have an answer added.");
                    pstmt.close();
                    return true;
                }

                sender.sendMessage(ChatColor.DARK_GREEN + rs.getString("question"));
                sender.sendMessage(ChatColor.GOLD + rs.getString("answer"));

                TextComponent survey = new TextComponent(ChatColor.GREEN + "Did this help?");
                TextComponent yes = new TextComponent(" [Yes]");
                yes.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/faq survey yes " + rs.getString("questionId")));
                TextComponent no = new TextComponent(" [No]");
                no.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/faq survey no " + rs.getString("questionId")));
                TextComponent bad = new TextComponent(" [I wasn't asking about that]");
                bad.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/faq survey bad " + rs.getString("questionId")));

                survey.addExtra(yes);
                survey.addExtra(no);
                survey.addExtra(bad);

                player.spigot().sendMessage(survey);

                pstmt.close();
                return true;
            } catch (Exception e) {
                plugin.getLogger().severe("/faq question: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                return true;
            }
        }
        return true;
    }
}
