package com.calderaminecraft.calderatips;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class CalderaTips extends JavaPlugin {
    private CalderaTips plugin;
    private File dbFile;
    private Connection conn = null;
    private Random rng = new Random();
    Map<String, String> triggerCache = new HashMap<>();
    Map<String, String> cmdCache = new HashMap<>();
    List<UUID> disabledUsers = new ArrayList<>();
    List<String> categoryIds = new ArrayList<>();
    List<String> questionIds = new ArrayList<>();
    List<String> triggerIds = new ArrayList<>();
    List<String> cmdTriggerIds = new ArrayList<>();

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        dbFile = new File(plugin.getDataFolder(), "CalderaTips.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("File write error: CalderaTips.db");
                return;
            }
        }

        try {
            Connection c = plugin.getConnection();
            Statement stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS tips (parentId TEXT, categoryId TEXT, categoryName TEXT, questionId TEXT, question TEXT, answer TEXT, voteyes INTEGER, voteno INTEGER, votebad INTEGER, priority INTEGER)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS triggers (triggerId TEXT, categoryId TEXT, questionId TEXT, regex TEXT)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS commandTriggers (triggerId TEXT, questionId TEXT, command TEXT)");

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS disabledUsers (uuid TEXT)");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM disabledUsers");
            while (rs.next()) {
                disabledUsers.add(UUID.fromString(rs.getString("uuid")));
            }

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT * FROM tips");
            while (rs.next()) {
                if (rs.getString("question") == null) {
                    categoryIds.add(rs.getString("categoryId"));
                } else {
                    questionIds.add(rs.getString("questionId"));
                }
            }

            stmt = c.createStatement();
            rs = stmt.executeQuery("PRAGMA user_version");

            int dbVersion = rs.getInt("user_version");
            plugin.getLogger().info("db version: " + dbVersion);

            if (dbVersion == 0) {
                plugin.getLogger().info("Upgrading database from version 0 to version 1.");
                stmt = c.createStatement();
                stmt.executeUpdate("ALTER TABLE tips ADD COLUMN priority INTEGER");

                stmt = c.createStatement();
                stmt.executeUpdate("UPDATE tips SET priority=100 WHERE priority IS NULL");

                stmt = c.createStatement();
                stmt.executeUpdate("PRAGMA user_version=1");
            }

            stmt.close();
        } catch (Exception e) {
            getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            getLogger().warning("CalderaTips failed to load.");
            return;
        }

        cacheRegexes();
        cacheCommands();

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);

        this.getCommand("calderatips").setExecutor(new CommandCalderaTips(this));
        this.getCommand("faq").setExecutor(new CommandFaq(this));

        this.getLogger().info("CalderaTips loaded");
    }

    @Override
    public void onDisable() {
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception ignored) {}
        }
    }

     void cacheIds() {
        try {
            plugin.categoryIds = new ArrayList<>();
            plugin.questionIds = new ArrayList<>();
            Connection c = plugin.getConnection();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM tips");
            while (rs.next()) {
                if (rs.getString("question") == null) {
                    categoryIds.add(rs.getString("categoryId"));
                } else {
                    questionIds.add(rs.getString("questionId"));
                }
            }

            stmt.close();
        } catch (Exception e) {

        }
    }

    void cacheCommands() {
        cmdCache = new HashMap<>();
        cmdTriggerIds = new ArrayList<>();
        try {
            Connection c = plugin.getConnection();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) FROM commandTriggers");

            if (rs.getInt("count(*)") < 1) return;

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT * FROM commandTriggers");
            while (rs.next()) {
                cmdCache.put(rs.getString("command"), rs.getString("questionId"));
                cmdTriggerIds.add(rs.getString("triggerId"));
            }

        } catch (Exception e) {
            plugin.getLogger().severe("cacheCommands: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }

     void cacheRegexes() {
        triggerCache = new HashMap<>();
        triggerIds = new ArrayList<>();
        try {
            Connection c = plugin.getConnection();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) FROM triggers");

            if (rs.getInt("count(*)") < 1) return;

            stmt = c.createStatement();
            rs = stmt.executeQuery("SELECT * FROM triggers");

            while (rs.next()) {
                if (rs.getString("regex") == null) continue;
                triggerCache.put(rs.getString("regex"), rs.getString("questionId"));
                triggerIds.add(rs.getString("triggerId"));
            }

        } catch (Exception e) {
            plugin.getLogger().severe("cacheRegexes: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }

     Connection getConnection() throws Exception {
        if (conn == null ) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + plugin.dbFile);
        }
        return conn;
    }

     String randomId() {
        String str = "";
        String characters = "abcdefghijklmnopqrstuvWxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (int i = 0; i < 6; i++) str += characters.charAt(rng.nextInt(characters.length()));
        return str;
    }

     List<BaseComponent> pageify(List messages, int pageNumber, String pageCmd) {
        int pageSize = 8;
        List<BaseComponent> page = new ArrayList<>();

        int totalPages = messages.size() / pageSize + ((messages.size() % pageSize == 0) ? 0 : 1);
        if (totalPages < pageNumber) pageNumber = totalPages;

        int count = 0;
        int curPageNum;

        page.add(new TextComponent(ChatColor.DARK_GREEN + "Page " + ChatColor.GOLD + pageNumber + ChatColor.DARK_GREEN + " of " + ChatColor.GOLD + totalPages));

        for (Object curPage : messages) {
            count++;
            curPageNum = count / pageSize + ((count % pageSize == 0) ? 0 : 1);
            if (curPageNum != pageNumber) continue;
            if (!(curPage instanceof BaseComponent)) curPage = new TextComponent((String) curPage);
            page.add((BaseComponent) curPage);
        }

        TextComponent footer = new TextComponent();
        TextComponent backButton = new TextComponent(ChatColor.DARK_GREEN + "[Last Page]");
        if (pageNumber > 1) backButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, pageCmd + " " + (pageNumber - 1)));
        TextComponent nextButton = new TextComponent(ChatColor.DARK_GREEN + "[Next Page]");
        if (pageNumber < totalPages) nextButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, pageCmd + " " + (pageNumber + 1)));

        footer.addExtra(backButton);
        footer.addExtra(" | ");
        footer.addExtra(nextButton);

        page.add(footer);

        return page;
    }
}
