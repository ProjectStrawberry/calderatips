package com.calderaminecraft.calderatips;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.regex.Pattern;

public class CommandCalderaTips implements TabExecutor {
    private List<UUID> confirmDeletion = new ArrayList<>();
    private CalderaTips plugin;

    CommandCalderaTips(CalderaTips p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            args = new String[]{"help"};
        }

        if (args[0].equals("help") || args[0].equals("?")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            int pageNum = 1;
            if (args.length > 1) {
                try {
                    pageNum = Integer.parseInt(args[1]);
                    if (pageNum < 1) pageNum = 1;
                } catch (NumberFormatException ignored) {}
            }

            List<TextComponent> messages = new ArrayList<>();
            messages.add(formatText("/faq - Lists all the question categories."));
            messages.add(formatText("/calderatips toggle - Toggles the auto responses."));
            if (sender.hasPermission("caldera.tips.admin")) {
                messages.add(formatText("/calderatips addcategory <parent id> <category name> - Creates a new category."));
                messages.add(formatText("/calderatips addquestion <category id> <question> - Adds a question to a category."));
                messages.add(formatText("/calderatips addanswer <question id> <answer> - Adds the answer to a question."));
                messages.add(formatText("/calderatips removeanswer <question id> - Removes an answer from a question."));
                messages.add(formatText("/calderatips addtrigger <question id> <word1 word2 word3 etc> - Adds a trigger for the auto response to this question."));
                messages.add(formatText("/calderatips addcmdtrigger <question id> <command> - Adds a command trigger for the auto response to this question."));
                messages.add(formatText("/calderatips priority <id> <number> - Sets the priority for a question or category. Lower numbers sort higher."));
                messages.add(formatText("/calderatips survey <question id> - Shows the survey responses for a question."));
                messages.add(formatText("/calderatips listtriggers <question id> - Lists all triggers added to a question."));
                messages.add(formatText("/calderatips listcmdtriggers <question id> - Lists all command triggers added to a question."));
                messages.add(formatText("/calderatips removecategory <category id> - Removes a category."));
                messages.add(formatText("/calderatips removequestion <question id> - Removes a question."));
                messages.add(formatText("/calderatips removetrigger <trigger id> - Removes a trigger from a question."));
                messages.add(formatText("/calderatips removecmdtrigger <trigger id> - Removes a command trigger"));
            }

            List<BaseComponent> pages = plugin.pageify(messages, pageNum, "/calderatips help");

            for (BaseComponent page : pages) player.spigot().sendMessage(page);

            return true;
        }

        if (args[0].equals("toggle")) {
            if (!sender.hasPermission("caldera.tips.use")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt;
                if (plugin.disabledUsers.contains(player.getUniqueId())) {
                    plugin.disabledUsers.remove(player.getUniqueId());
                    pstmt = c.prepareStatement("DELETE FROM disabledUsers WHERE uuid=?");
                    pstmt.setString(1, player.getUniqueId().toString());
                    pstmt.executeUpdate();
                    pstmt.close();
                    sender.sendMessage(ChatColor.DARK_GREEN + "You've enabled CalderaTips messages.");
                    return true;
                } else {
                    plugin.disabledUsers.add(player.getUniqueId());
                    pstmt = c.prepareStatement("INSERT INTO disabledUsers (uuid) VALUES (?)");
                    pstmt.setString(1, player.getUniqueId().toString());
                    pstmt.executeUpdate();
                    pstmt.close();
                    sender.sendMessage(ChatColor.DARK_GREEN + "You've disabled CalderaTips messages.");
                    return true;
                }
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips toggle: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (!sender.hasPermission("caldera.tips.admin")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        if (args[0].equals("cancel")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            if (!confirmDeletion.contains(player.getUniqueId())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have any pending deletions to cancel.");
            }

            confirmDeletion.remove(player.getUniqueId());
            sender.sendMessage(ChatColor.DARK_GREEN + "You have cancelled your pending deletion.");
            return true;
        }

        if (args[0].equals("survey")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips survey <question id>");
                return true;
            }

            String questionId = args[1];

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no question with that id.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                rs = pstmt.executeQuery();

                sender.sendMessage(ChatColor.DARK_GREEN + "Survey results:");
                sender.sendMessage(ChatColor.GOLD + rs.getString("question"));
                sender.sendMessage(ChatColor.GREEN + "Was this helpful?");
                sender.sendMessage(ChatColor.DARK_GREEN + "Yes: " + ChatColor.GOLD + rs.getInt("voteyes"));
                sender.sendMessage(ChatColor.DARK_GREEN + "No: " + ChatColor.GOLD + rs.getInt("voteno"));
                sender.sendMessage(ChatColor.DARK_GREEN + "I wasn't asking about this: " + ChatColor.GOLD + rs.getInt("votebad"));

                pstmt.close();
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips survey: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("removecategory")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips removecategory <category id>");
                return true;
            }

            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            String categoryId = args[1];

            try {
                Connection c = plugin.getConnection();

                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE categoryId=? OR parentId=?");
                pstmt.setString(1, categoryId);
                pstmt.setString(2, categoryId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no categories with that id.");
                    pstmt.close();
                    return true;
                }

                if (!confirmDeletion.contains(player.getUniqueId())) {
                    sender.sendMessage(ChatColor.RED + "Are you sure you want to remove this category?");
                    TextComponent confirm = new TextComponent("[Yes]");
                    confirm.setColor(ChatColor.DARK_GREEN.asBungee());
                    confirm.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removecategory " + categoryId));

                    TextComponent cancel = new TextComponent("[Cancel]");
                    cancel.setColor(ChatColor.DARK_RED.asBungee());
                    cancel.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips cancel"));

                    TextComponent reply = new TextComponent();
                    reply.addExtra(confirm);
                    reply.addExtra(" ");
                    reply.addExtra(cancel);
                    player.spigot().sendMessage(reply);
                    pstmt.close();
                    confirmDeletion.add(player.getUniqueId());
                    return true;
                }

                confirmDeletion.remove(player.getUniqueId());

                pstmt = c.prepareStatement("DELETE FROM tips WHERE categoryId=? OR parentId=?");
                pstmt.setString(1, categoryId);
                pstmt.setString(2, categoryId);
                pstmt.executeUpdate();

                pstmt = c.prepareStatement("DELETE FROM triggers WHERE categoryId=?");
                pstmt.setString(1, categoryId);
                pstmt.executeUpdate();
                pstmt.close();

                plugin.cacheRegexes();
                plugin.cacheIds();

                sender.sendMessage(ChatColor.DARK_GREEN + "That category has been removed.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips removecategory: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("removequestion")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips removequestion <question id>");
                return true;
            }

            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            String questionId = args[1];

            try {
                Connection c = plugin.getConnection();

                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no questions with that id.");
                    pstmt.close();
                    return true;
                }

                if (!confirmDeletion.contains(player.getUniqueId())) {
                    sender.sendMessage(ChatColor.RED + "Are you sure you want to remove this question?");
                    TextComponent confirm = new TextComponent("[Yes]");
                    confirm.setColor(ChatColor.DARK_GREEN.asBungee());
                    confirm.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removequestion " + questionId));

                    TextComponent cancel = new TextComponent("[Cancel]");
                    cancel.setColor(ChatColor.DARK_RED.asBungee());
                    cancel.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips cancel"));

                    TextComponent reply = new TextComponent();
                    reply.addExtra(confirm);
                    reply.addExtra(" ");
                    reply.addExtra(cancel);
                    player.spigot().sendMessage(reply);
                    pstmt.close();
                    confirmDeletion.add(player.getUniqueId());
                    return true;
                }

                confirmDeletion.remove(player.getUniqueId());

                pstmt = c.prepareStatement("DELETE FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                pstmt.executeUpdate();

                pstmt = c.prepareStatement("DELETE FROM triggers WHERE questionId=?");
                pstmt.setString(1, questionId);
                pstmt.executeUpdate();
                pstmt.close();

                plugin.cacheRegexes();
                plugin.questionIds.remove(questionId);

                sender.sendMessage(ChatColor.DARK_GREEN + "That question has been removed.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips removequestion: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("removetrigger")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips removetrigger <trigger id>");
                return true;
            }

            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            String triggerId = args[1];

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM triggers WHERE triggerId=?");
                pstmt.setString(1, triggerId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A trigger with that id does not exist.");
                    pstmt.close();
                    return true;
                }

                if (!confirmDeletion.contains(player.getUniqueId())) {
                    sender.sendMessage(ChatColor.DARK_GREEN + "Are you sure you want to remove this trigger?");
                    TextComponent confirm = new TextComponent("[Yes]");
                    confirm.setColor(ChatColor.GREEN.asBungee());
                    confirm.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removetrigger " + triggerId));

                    TextComponent cancel = new TextComponent("[Cancel]");
                    cancel.setColor(ChatColor.DARK_RED.asBungee());
                    cancel.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips cancel"));

                    TextComponent reply = new TextComponent();
                    reply.addExtra(confirm);
                    reply.addExtra(" ");
                    reply.addExtra(cancel);
                    player.spigot().sendMessage(reply);
                    pstmt.close();
                    confirmDeletion.add(player.getUniqueId());
                    return true;
                }

                confirmDeletion.remove(player.getUniqueId());

                pstmt = c.prepareStatement("DELETE FROM triggers WHERE triggerId=?");
                pstmt.setString(1, triggerId);
                pstmt.executeUpdate();
                pstmt.close();

                plugin.cacheRegexes();

                sender.sendMessage(ChatColor.DARK_GREEN + "That trigger has been removed.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips removetrigger: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("listtriggers")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips listtriggers <question id>");
                return true;
            }

            Player player = (Player) sender;

            String questionId = args[1];
            int pageNum = 1;

            if (args.length > 3) {
                try {
                    pageNum = Integer.parseInt(args[3]);
                    if (pageNum < 1) pageNum = 1;
                } catch (NumberFormatException ignored) {}
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM triggers WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no triggers added to this question.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM triggers WHERE questionId=?");
                pstmt.setString(1, questionId);
                rs = pstmt.executeQuery();

                List<BaseComponent> messages = new ArrayList<>();

                while (rs.next()) {
                    TextComponent message = new TextComponent(ChatColor.DARK_GREEN + "Trigger: " + ChatColor.GOLD + rs.getString("regex") + ChatColor.DARK_GREEN + " ID: " + ChatColor.GOLD + rs.getString("triggerId"));
                    message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removetrigger " + rs.getString("triggerId")));
                    messages.add(message);
                }

                List<BaseComponent> pages = plugin.pageify(messages, pageNum, "/calderatips listtriggers " + questionId);

                for (BaseComponent curPage : pages) {
                    player.spigot().sendMessage(curPage);
                }

                pstmt.close();
                return true;
             } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips listtriggers: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("removecmdtrigger")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips removecmdtrigger <trigger id>");
                return true;
            }

            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            String triggerId = args[1];

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM commandTriggers WHERE triggerId=?");
                pstmt.setString(1, triggerId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A command trigger with that id does not exist.");
                    pstmt.close();
                    return true;
                }

                if (!confirmDeletion.contains(player.getUniqueId())) {
                    sender.sendMessage(ChatColor.DARK_GREEN + "Are you sure you want to remove this command trigger?");
                    TextComponent confirm = new TextComponent("[Yes]");
                    confirm.setColor(ChatColor.GREEN.asBungee());
                    confirm.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removecmdtrigger " + triggerId));

                    TextComponent cancel = new TextComponent("[Cancel]");
                    cancel.setColor(ChatColor.DARK_RED.asBungee());
                    cancel.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips cancel"));

                    TextComponent reply = new TextComponent();
                    reply.addExtra(confirm);
                    reply.addExtra(" ");
                    reply.addExtra(cancel);
                    player.spigot().sendMessage(reply);
                    pstmt.close();
                    confirmDeletion.add(player.getUniqueId());
                    return true;
                }

                confirmDeletion.remove(player.getUniqueId());

                pstmt = c.prepareStatement("DELETE FROM commandTriggers WHERE triggerId=?");
                pstmt.setString(1, triggerId);
                pstmt.executeUpdate();
                pstmt.close();

                plugin.cacheCommands();

                sender.sendMessage(ChatColor.DARK_GREEN + "That command trigger has been removed.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips removecmdtrigger: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("listcmdtriggers")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips listcmdtriggers <question id>");
                return true;
            }

            Player player = (Player) sender;

            String questionId = args[1];
            int pageNum = 1;

            if (args.length > 3) {
                try {
                    pageNum = Integer.parseInt(args[3]);
                    if (pageNum < 1) pageNum = 1;
                } catch (NumberFormatException ignored) {}
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM commandTriggers WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There are no triggers added to this question.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM commandTriggers WHERE questionId=?");
                pstmt.setString(1, questionId);
                rs = pstmt.executeQuery();

                List<BaseComponent> messages = new ArrayList<>();

                while (rs.next()) {
                    TextComponent message = new TextComponent(ChatColor.DARK_GREEN + "Trigger: " + ChatColor.GOLD + rs.getString("command") + ChatColor.DARK_GREEN + " ID: " + ChatColor.GOLD + rs.getString("triggerId"));
                    message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/calderatips removecmdtrigger " + rs.getString("triggerId")));
                    messages.add(message);
                }

                List<BaseComponent> pages = plugin.pageify(messages, pageNum, "/calderatips listcmdtriggers " + questionId);

                for (BaseComponent curPage : pages) {
                    player.spigot().sendMessage(curPage);
                }

                pstmt.close();
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips listcmdtriggers: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("addcmd") || args[0].equals("addcmdtrigger")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips addcmd <question id> <command>");
                return true;
            }
            if (!sender.hasPermission("caldera.tips.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            String questionId = args[1];
            String trigger = args[2];
            String triggerId = plugin.randomId();

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That question does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("INSERT INTO commandTriggers (triggerId, questionId, command) VALUES (?, ?, ?)");
                pstmt.setString(1, triggerId);
                pstmt.setString(2, questionId);
                pstmt.setString(3, trigger);
                pstmt.executeUpdate();

                pstmt.close();
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips addcmd: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }

            plugin.cacheCommands();
            sender.sendMessage(ChatColor.DARK_GREEN + "You've added a command trigger to that question with the id " + ChatColor.GOLD + triggerId);
            return true;
        }

        if (args[0].equals("addtrigger")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips addtrigger <question id> <trigger>");
                return true;
            }
            if (!sender.hasPermission("caldera.tips.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            String questionId = args[1];
            String trigger = "^";
            String triggerId = plugin.randomId();

            for (int i=2; i<args.length; i++) {
                trigger += "(?=.*\\b" + Pattern.quote(args[i].toLowerCase()) + "\\b)";
            }

            trigger += ".*$";

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That question does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("INSERT INTO triggers (triggerId, questionId, regex) VALUES (?, ?, ?)");
                pstmt.setString(1, triggerId);
                pstmt.setString(2, questionId);
                pstmt.setString(3, trigger);
                pstmt.executeUpdate();

                pstmt.close();
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips addtrigger: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }

            plugin.cacheRegexes();
            sender.sendMessage(ChatColor.DARK_GREEN + "You've added a trigger to that question with the id " + ChatColor.GOLD + triggerId);
            return true;
        }

        if (args[0].equals("addanswer")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips addanswer <question id> <answer>");
                return true;
            }
            if (!sender.hasPermission("caldera.tips.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            String questionId = args[1];
            String answer = "";

            for (int i=2; i<args.length; i++) answer += args[i] + " ";

            answer = answer.trim();

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That question does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("SELECT * FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                rs = pstmt.executeQuery();

                String existingAnswer = rs.getString("answer");

                if (existingAnswer != null) {
                    answer = existingAnswer + " " + answer;
                }

                pstmt = c.prepareStatement("UPDATE tips SET answer=? WHERE questionId=?");
                pstmt.setString(1, answer);
                pstmt.setString(2, questionId);
                pstmt.executeUpdate();
                pstmt.close();

                sender.sendMessage(ChatColor.DARK_GREEN + "You've added the answer for that question.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips addanswer: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("removeanswer")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips removeanswer <question id>");
                return true;
            }
            if (!sender.hasPermission("caldera.tips.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            String questionId = args[1];

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                pstmt.setString(1, questionId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That question does not exist.");
                    pstmt.close();
                    return true;
                }

                pstmt = c.prepareStatement("UPDATE tips SET answer=NULL WHERE questionId=?");
                pstmt.setString(1, questionId);
                pstmt.executeUpdate();
                pstmt.close();

                sender.sendMessage(ChatColor.DARK_GREEN + "You've removed the answer for that question.");
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips addanswer: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("priority")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips priority <question id> <priority>");
                return true;
            }
            if (!sender.hasPermission("caldera.tips.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            String id = args[1];
            int priority;
            try {
                priority = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid number.");
                return true;
            }

            boolean isCategory = false;

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE questionId=?");
                pstmt.setString(1, id);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE categoryId=? AND questionId IS NULL");
                    pstmt.setString(1, id);
                    rs = pstmt.executeQuery();
                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That id does not exist.");
                        pstmt.close();
                        return true;
                    }
                    isCategory = true;
                }

                String sql = "UPDATE tips SET priority=? WHERE " + (isCategory ? "categoryId=? AND questionId IS NULL" : "questionId=?");

                pstmt = c.prepareStatement("UPDATE tips SET priority=? WHERE " + (isCategory ? "categoryId=? AND questionId IS NULL" : "questionId=?"));
                pstmt.setInt(1, priority);
                pstmt.setString(2, id);
                pstmt.executeUpdate();
                pstmt.close();

                sender.sendMessage(ChatColor.DARK_GREEN + "You've set the priority on that " + (isCategory ? "category" : "question") + " to " + ChatColor.GOLD + priority);
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips priority: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("addquestion")) {
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips addquestion <category id> <question>");
                return true;
            }
            if (!sender.hasPermission("caldera.tips.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            String categoryId = args[1];
            String question = "";

            for (int i=2; i<args.length;i++) question += args[i] + " ";

            question = question.trim();

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE categoryId=?");
                pstmt.setString(1, categoryId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That category does not exist.");
                    pstmt.close();
                    return true;
                }

                String questionId = plugin.randomId();
                pstmt = c.prepareStatement("INSERT INTO tips (categoryId, questionId, question, voteyes, voteno, votebad, priority) VALUES (?, ?, ?, 0, 0, 0, 100)");
                pstmt.setString(1, categoryId);
                pstmt.setString(2, questionId);
                pstmt.setString(3, question);
                pstmt.executeUpdate();

                plugin.questionIds.add(questionId);

                sender.sendMessage(ChatColor.DARK_GREEN + "You've added a new question with the id " + ChatColor.GOLD + questionId);
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips new: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        if (args[0].equals("addcategory")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderatips addcategory <parentId> <categoryname>");
                return true;
            }
            if (!sender.hasPermission("caldera.tips.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            String parentId = args[1];
            String categoryName = "";

            for (int i=2; i<args.length; i++) categoryName += args[i] + " ";

            categoryName = categoryName.trim();

            String categoryId = plugin.randomId();

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE categoryName=?");
                pstmt.setString(1, categoryName);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") > 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That category already exists.");
                    pstmt.close();
                    return true;
                }

                if (!parentId.equals("root")) {
                    pstmt = c.prepareStatement("SELECT count(*) FROM tips WHERE categoryId=?");
                    pstmt.setString(1, parentId);
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That parent category does not exist.");
                        pstmt.close();
                        return true;
                    }
                }

                pstmt = c.prepareStatement("INSERT INTO tips (categoryName, categoryId, parentId, priority) VALUES (?, ?, ?, 100)");
                pstmt.setString(1, categoryName);
                pstmt.setString(2, categoryId);
                pstmt.setString(3, parentId);
                pstmt.executeUpdate();

                sender.sendMessage(ChatColor.DARK_GREEN + "You've created a new category. Its id is " + ChatColor.GOLD + categoryId);
                pstmt.close();
                plugin.categoryIds.add(categoryId);
                return true;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/calderatips newcategory: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        return true;
    }

    private TextComponent formatText(String message) {
        TextComponent component = new TextComponent(message);
        component.setColor(ChatColor.GOLD.asBungee());
        return component;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) {
            tabComplete.add("toggle");
            if (sender.hasPermission("caldera.tips.admin")) {
                tabComplete.add("addcategory");
                tabComplete.add("addquestion");
                tabComplete.add("addanswer");
                tabComplete.add("removeanswer");
                tabComplete.add("addtrigger");
                tabComplete.add("addcmdtrigger");
                tabComplete.add("listtriggers");
                tabComplete.add("listcmdtriggers");
                tabComplete.add("removecategory");
                tabComplete.add("removequestion");
                tabComplete.add("removetrigger");
                tabComplete.add("removecmdtrigger");
                tabComplete.add("survey");
                tabComplete.add("priority");
            }
        }

        if (args.length == 2) {
            if (args[0].equals("addcategory")) {
                tabComplete.addAll(plugin.categoryIds);
                tabComplete.add("root");
            }
            if (args[0].equals("addquestion")) tabComplete.addAll(plugin.categoryIds);
            if (args[0].equals("addanswer")) tabComplete.addAll(plugin.questionIds);
            if (args[0].equals("removeanswer")) tabComplete.addAll(plugin.questionIds);
            if (args[0].equals("addtrigger")) tabComplete.addAll(plugin.questionIds);
            if (args[0].equals("addcmdtrigger") || args[0].equals("addcmd")) tabComplete.addAll(plugin.questionIds);
            if (args[0].equals("listtriggers")) tabComplete.addAll(plugin.questionIds);
            if (args[0].equals("listcmdtriggers")) tabComplete.addAll(plugin.questionIds);
            if (args[0].equals("removecategory")) tabComplete.addAll(plugin.categoryIds);
            if (args[0].equals("removequestion")) tabComplete.addAll(plugin.questionIds);
            if (args[0].equals("removetrigger")) tabComplete.addAll(plugin.triggerIds);
            if (args[0].equals("removecmdtrigger")) tabComplete.addAll(plugin.cmdTriggerIds);
            if (args[0].equals("survey")) tabComplete.addAll(plugin.questionIds);
            if (args[0].equals("priority")) {
                tabComplete.addAll(plugin.questionIds);
                tabComplete.addAll(plugin.categoryIds);
            }
        }

        if (args.length == 3) {
            if (args[0].equals("addcategory")) tabComplete.add("<category name>");
            if (args[0].equals("addanswer")) tabComplete.add("<answer>");
            if (args[0].equals("addtrigger")) tabComplete.add("<word1 word2 word3 etc>");
            if (args[0].equals("addcmdtrigger") || args[0].equals("addcmd")) tabComplete.add("<command>");
            if (args[0].equals("priority")) tabComplete.add("<number>");
        }

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<String>()) : null;
    }
}
